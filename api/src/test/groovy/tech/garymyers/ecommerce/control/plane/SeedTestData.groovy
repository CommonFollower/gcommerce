package tech.garymyers.ecommerce.control.plane;

import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Requires
import spock.lang.Specification
import tech.garymyers.ecommerce.control.plane.api.company.Company
import tech.garymyers.ecommerce.control.plane.api.company.CompanyTestDataLoaderService
import tech.garymyers.ecommerce.control.plane.api.security.SecurityUserTestDataLoaderService
import tech.garymyers.ecommerce.control.plane.api.security.user.SecurityUser
import tech.garymyers.ecommerce.control.plane.domain.TruncateDatabaseService

import javax.inject.Inject

@Requires({ System.getenv("SEED_DATA")?.equalsIgnoreCase("true") })
@MicronautTest(transactional = false, environments = "load")
class SeedTestData extends Specification {
   @Inject CompanyTestDataLoaderService companyTestDataLoaderService
   @Inject TruncateDatabaseService truncateDatabaseService
   @Inject SecurityUserTestDataLoaderService securityUserTestDataLoaderService

   void setup() {
      truncateDatabaseService.truncate()
   }

   void "load data" () {
      given:
      final List<Company> companies = companyTestDataLoaderService.load(2).toList()
      final List<SecurityUser> users = companies.stream().flatMap {securityUserTestDataLoaderService.load(2, it) }.peek { println(it) }.toList()

      expect:
      companies.size() == 2
      users.size() == 4

      cleanup:
      users.each {
         println it.username + " - " + it.password
      }
   }
}
