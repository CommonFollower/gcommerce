package tech.garymyers.ecommerce.control.plane.api.security.user

import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity
import io.micronaut.data.annotation.Relation
import io.micronaut.data.annotation.Relation.Kind.MANY_TO_ONE
import io.micronaut.data.annotation.Relation.Kind.ONE_TO_MANY
import tech.garymyers.domain.Identifiable
import tech.garymyers.ecommerce.control.plane.api.company.Company
import tech.garymyers.ecommerce.control.plane.api.security.role.SecurityRoleTypeEntity
import java.time.OffsetDateTime
import java.util.*
import kotlin.collections.LinkedHashSet

@MappedEntity("security_user")
data class SecurityUser (

   @field:Id
   @field:GeneratedValue
   val id: UUID? = null,

   @field:GeneratedValue
   var timeCreated: OffsetDateTime? = null,

   @field:GeneratedValue
   var timeUpdated: OffsetDateTime? = null,

   val username: String,

   val password: String,

   @field:Relation(value = MANY_TO_ONE)
   val company: Company,

   @field:Relation(value = ONE_TO_MANY)
   val roles: LinkedHashSet<SecurityRoleTypeEntity> = LinkedHashSet()

) : Identifiable {
   override fun myId(): UUID? = id
}
