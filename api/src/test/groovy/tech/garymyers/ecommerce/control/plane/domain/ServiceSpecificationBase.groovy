package tech.garymyers.ecommerce.control.plane.domain

import spock.lang.Specification

import javax.inject.Inject

class ServiceSpecificationBase extends Specification {
    @Inject TruncateDatabaseService truncateDatabaseService

    void setup() {
        truncateDatabaseService.truncate()
    }
}
