package tech.garymyers.ecommerce.control.plane.api.company.infrastructure

import io.micronaut.http.HttpStatus
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import tech.garymyers.ecommerce.control.plane.api.company.CompanyDto
import tech.garymyers.ecommerce.control.plane.api.company.CompanyTestDataLoader
import tech.garymyers.ecommerce.control.plane.api.security.role.Roles
import tech.garymyers.ecommerce.control.plane.domain.ControllerSpecificationBase

@MicronautTest(transactional = false)
class CompanyControllerSpecification extends ControllerSpecificationBase {

   void 'fetch one'() {
      given:
      final company = companyTestDataLoaderService.single()

      when:
      def result = get("/company/${company.getId()}")

      then:
      notThrown(Exception)
      with(result) {
         id == company.id
         name == company.name
      }
   }

   void 'create company as an admin' () {
      given:
      final company = CompanyTestDataLoader.single().with { new CompanyDto(it) }

      when:
      def result = post("/company", company)

      then:
      notThrown(Exception)
      with(result) {
         id != null
         name == company.name
      }
   }

   void 'create company as a company admin' () {
      given:
      final company = CompanyTestDataLoader.single().with { new CompanyDto(it) }
      final companyAdminUser = securityUserTestDataLoaderService.single(super.company, [Roles.COMPANY_ADMIN])
      final companyAdminUserToken = loginEmployee(companyAdminUser)

      when:
      post("/company", company, companyAdminUserToken)

      then:
      def ex = thrown(HttpClientResponseException)
      ex.status == HttpStatus.FORBIDDEN
   }

   void 'update company as an admin' () {
      given:
      final company = companyTestDataLoaderService.single().with {new CompanyDto(it.id, "Test company") }

      when:
      def result = put("/company/${company.id}", company)

      then:
      notThrown(Exception)
      with(result) {
         id == company.id
         name == "Test company"
      }
   }

   void 'update company as a warehouse picker' () {
      given:
      final company = companyTestDataLoaderService.single().with {new CompanyDto(it.id, "Test company") }
      final warehousePickerUser = securityUserTestDataLoaderService.single(super.company, [Roles.WAREHOUSE_PICKER])
      final warehousePickerUserToken = loginEmployee(warehousePickerUser)

      when:
      put("/company/${company.id}", company, warehousePickerUserToken)

      then:
      def ex = thrown(HttpClientResponseException)
      ex.status == HttpStatus.FORBIDDEN
   }
}
