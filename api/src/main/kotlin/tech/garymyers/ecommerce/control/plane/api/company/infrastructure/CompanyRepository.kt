package tech.garymyers.ecommerce.control.plane.api.company.infrastructure

import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.repository.PageableRepository
import tech.garymyers.ecommerce.control.plane.api.company.Company
import java.util.*

@JdbcRepository
interface CompanyRepository : PageableRepository<Company, UUID>
