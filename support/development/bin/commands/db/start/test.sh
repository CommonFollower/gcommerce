#!/usr/bin/env bash
## description: starts thenigeltestdb database makes it available on localhost:6432 and returns once they are up and running

cd ../docker

daemon="-d"

if [ "$1" == "--nodaemon" ]; then
  daemon=""
fi

if [ -z `docker-compose ps -q ecomtestdb` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q ecomtestdb)` ]; then
  docker rm ecomtestdb > /dev/null 2>&1
  docker-compose build --force-rm --quiet ecombasedb
  docker-compose build --force-rm --quiet ecomtestdb
  docker-compose up $daemon --no-deps ecomtestdb

  if [ "$daemon" == "-d" ]; then
    docker-compose build --force-rm --quiet ecomtestdbready && docker-compose run --no-deps --rm ecomtestdbready
  fi

  exit $?
else
  echo "ecomtestdb is already running checking if it is accepting connections"
  docker-compose build --force-rm --quiet ecomtestdbready && docker-compose run --rm ecomtestdbready
  echo "can be accessed at $(docker-compose port ecomtestdb 5432)"
  exit 1
fi
