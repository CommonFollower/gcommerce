package tech.garymyers.ecommerce.control.plane.api.security

import io.micronaut.security.authentication.UsernamePasswordCredentials
import tech.garymyers.ecommerce.control.plane.api.security.user.SecurityUser
import java.util.UUID

class LoginCredentials(
   username: String? = null,
   password: String? = null,
   var companyId: UUID? = null
) : UsernamePasswordCredentials(
   username,
   password
) {
   constructor(authenticatedUser: SecurityUser) :
      this(
         username = authenticatedUser.username,
         password = authenticatedUser.password,
         companyId = authenticatedUser.id,
      )
}
