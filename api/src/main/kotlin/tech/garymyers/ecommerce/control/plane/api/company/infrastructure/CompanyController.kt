package tech.garymyers.ecommerce.control.plane.api.company.infrastructure

import io.micronaut.http.HttpStatus.CREATED
import io.micronaut.http.MediaType.APPLICATION_JSON
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Put
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.annotation.Status
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule.IS_ANONYMOUS
import io.micronaut.validation.Validated
import tech.garymyers.ecommerce.control.plane.api.company.Company
import tech.garymyers.ecommerce.control.plane.api.company.CompanyDto
import tech.garymyers.ecommerce.control.plane.api.company.CompanyService
import tech.garymyers.ecommerce.control.plane.domain.ContollerPage
import tech.garymyers.ecommerce.control.plane.domain.StandardControllerPageRequest
import tech.garymyers.ecommerce.control.plane.extensions.toControllerPage
import java.util.UUID
import javax.inject.Inject
import javax.validation.Valid

@Validated
@Secured(IS_ANONYMOUS)
@Controller("/api/company")
class CompanyController @Inject constructor(
   private val companyService: CompanyService,
) {

   @Get("/{id}", produces = [APPLICATION_JSON])
   fun readOne(
      @PathVariable("id") id: UUID
   ): CompanyDto? =
      companyService
         .findOne(id)
         ?.let { CompanyDto(it) }

   @Get(value = "{?pageRequest*}", produces = [APPLICATION_JSON])
   fun readAll(
      @QueryValue("pageRequest") pageRequest: StandardControllerPageRequest?,
   ): ContollerPage<CompanyDto> {
      val requested = pageRequest ?: StandardControllerPageRequest()

      return companyService
         .findAll(requested.toPageable())
         .toControllerPage(requested) { CompanyDto(it) }
   }

   @Status(CREATED)
   @Secured("ADMIN")
   @Post(processes = [APPLICATION_JSON])
   fun create(
      @Valid @Body
      companyDto: CompanyDto
   ): CompanyDto =
      CompanyDto(companyService.save(Company(companyDto)))

   @Secured("ADMIN")
   @Put("/{id}", processes = [APPLICATION_JSON])
   fun update(
      @PathVariable("id") id: UUID,
      @Valid @Body
      companyDto: CompanyDto
   ): CompanyDto =
      CompanyDto(companyService.update(Company(companyDto, id)))
}
