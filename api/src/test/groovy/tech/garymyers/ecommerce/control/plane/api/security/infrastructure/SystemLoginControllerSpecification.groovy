package tech.garymyers.ecommerce.control.plane.api.security.infrastructure

import groovy.json.JsonSlurper
import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import tech.garymyers.ecommerce.control.plane.api.company.CompanyTestDataLoaderService
import tech.garymyers.ecommerce.control.plane.api.security.LoginCredentials
import tech.garymyers.ecommerce.control.plane.api.security.SecurityUserTestDataLoaderService
import tech.garymyers.ecommerce.control.plane.api.security.role.Roles
import tech.garymyers.ecommerce.control.plane.domain.ServiceSpecificationBase

import javax.inject.Inject

import static tech.garymyers.ecommerce.control.plane.api.security.role.Roles.ADMIN

@MicronautTest(transactional = false)
class SystemLoginControllerSpecification  extends ServiceSpecificationBase {
   @Inject @Client("/api") RxHttpClient httpClient
   @Inject CompanyTestDataLoaderService companyTestDataLoaderService
   @Inject SecurityUserTestDataLoaderService userTestDataLoaderService

   void 'login successful' () {
      given:
      final company = companyTestDataLoaderService.single()
      final user = userTestDataLoaderService.single(company, [ADMIN])
      final decoder = Base64.getDecoder()
      final jsonSlurper = new JsonSlurper()

      when:
      def authResponse = httpClient.toBlocking()
         .exchange(
            HttpRequest.POST("/login", new LoginCredentials(user.username, user.password, company.id)),
            Argument.of(String),
            Argument.of(String),
         ).bodyAsJson()

      then:
      notThrown(Exception)
      with(authResponse) {
         access_token != null

         final tokenChunks = access_token.split("\\.")
         final header = new String(decoder.decode(tokenChunks[0])).with { jsonSlurper.parseText(it) }
         final payload = new String(decoder.decode(tokenChunks[1])).with {jsonSlurper.parseText(it) }

         header.alg == "HS256"
         payload.sub == user.username
         payload.companyId == company.id.toString()
         payload.userId == user.id.toString()
         payload.roles.size() == 1
         payload.roles[0] == "ADMIN"
      }
   }
}
