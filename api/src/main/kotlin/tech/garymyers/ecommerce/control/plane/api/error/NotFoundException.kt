package tech.garymyers.ecommerce.control.plane.api.error

import java.util.UUID

class NotFoundException(
   message: String
) : Exception(message) {
   constructor(id: UUID) :
      this(message = id.toString())
}
