package tech.garymyers.ecommerce.control.plane.api.security.user

import io.micronaut.security.authentication.Authentication
import tech.garymyers.ecommerce.control.plane.api.error.NotFoundException
import tech.garymyers.ecommerce.control.plane.api.security.user.infrastructure.SecurityUserRepository
import java.util.UUID
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserAuthenticationService @Inject constructor(
   private val userRepository: SecurityUserRepository
) {

   fun findUser(authentication: Authentication): SecurityUser? {
      val userId = authentication.attributes["userId"]?.let { UUID.fromString(it as String) } ?: throw NotFoundException("Unable to find logged in user")
      val companyId = authentication.attributes["companyId"]?.let { UUID.fromString(it as String) } ?: throw NotFoundException("Unable to find logged in user")

      return userRepository.findByIdAndCompanyId(userId, companyId)
   }
}
