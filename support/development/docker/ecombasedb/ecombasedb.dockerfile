FROM postgres:13.2-alpine

COPY pgpass /root/.pgpass
RUN chmod 0600 /root/.pgpass

COPY postgresql.conf /usr/local/share/postgresql/postgresql.conf.sample
COPY psqlrc /root/.psqlrc
COPY db-ready.sh /root/db-ready.sh

RUN chmod u+x /root/db-ready.sh

RUN apk add pspg