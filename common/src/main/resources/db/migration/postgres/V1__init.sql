CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE OR REPLACE FUNCTION user_table_update_fn()
    RETURNS TRIGGER AS
    $$
BEGIN
    IF new.id <> old.id THEN -- help ensure that the updated can't be updated once it is created
        RAISE EXCEPTION 'cannot update id once it has been created';
    END IF;

    new.time_updated := clock_timestamp();

    RETURN new;
END;
$$
LANGUAGE plpgsql;

CREATE TABLE company
(
    id           UUID        DEFAULT uuid_generate_v1() PRIMARY KEY NOT NULL,
    time_created TIMESTAMPTZ DEFAULT clock_timestamp()              NOT NULL,
    time_updated TIMESTAMPTZ DEFAULT clock_timestamp()              NOT NULL,
    name         VARCHAR(50) CHECK ( char_length(trim(name)) > 1 )  NOT NULL
);
CREATE TRIGGER update_company_trg
    BEFORE UPDATE
    ON company
    FOR EACH ROW
    EXECUTE PROCEDURE user_table_update_fn();

CREATE TABLE warehouse
(
    id           UUID        DEFAULT uuid_generate_v1() PRIMARY KEY     NOT NULL,
    time_created TIMESTAMPTZ DEFAULT clock_timestamp()                  NOT NULL,
    time_updated TIMESTAMPTZ DEFAULT clock_timestamp()                  NOT NULL,
    location     VARCHAR(150) CHECK ( char_length(trim(location)) > 1 ) NOT NULL,
    on_premises  BOOLEAN     DEFAULT FALSE                              NOT NULL,
    company_id   UUID REFERENCES company (id)                           NOT NULL,
    UNIQUE (location, company_id)
);
CREATE TRIGGER update_warehouse_trg
    BEFORE UPDATE
    ON warehouse
    FOR EACH ROW
    EXECUTE PROCEDURE user_table_update_fn();
CREATE INDEX warehouse__company_id_idx ON warehouse(company_id);

CREATE TABLE security_user
(
    id           UUID        DEFAULT uuid_generate_v1() PRIMARY KEY    NOT NULL,
    time_created TIMESTAMPTZ DEFAULT clock_timestamp()                 NOT NULL,
    time_updated TIMESTAMPTZ DEFAULT clock_timestamp()                 NOT NULL,
    username     VARCHAR(100) CHECK ( char_length(trim(username)) > 1) NOT NULL,
    password     TEXT CHECK ( char_length(trim(password)) > 1 )        NOT NULL,
    enabled      BOOLEAN     DEFAULT TRUE                              NOT NULL,
    company_id   UUID REFERENCES company (id)                          NOT NULL,
    UNIQUE (username, company_id, enabled)
);
CREATE TRIGGER update_security_user_trg
    BEFORE UPDATE
    ON security_user
    FOR EACH ROW
    EXECUTE PROCEDURE user_table_update_fn();
CREATE INDEX security_user__company_id_idx ON security_user (company_id);

CREATE TABLE security_role_type_domain
(
    id                INTEGER PRIMARY KEY NOT NULL,
    value             VARCHAR(20)         NOT NULL,
    description       VARCHAR(100)        NOT NULL,
    localization_code VARCHAR(100)        NOT NULL,
    UNIQUE (value)
);
INSERT INTO security_role_type_domain (id, value, description, localization_code)
VALUES (1, 'ADMIN', 'System Administrator', 'system.administrator'),
       (2, 'COMPANY_ADMIN', 'Company Administrator', 'company.administrator'),
       (3, 'WAREHOUSE_MANAGER', 'Warehouse Manager', 'warehouse.manager'),
       (4, 'WAREHOUSE_PICKER', 'Warehouse Picker', 'warehouse.picker');

CREATE TABLE security_user_role
(
    security_user_id      UUID    NOT NULL,
    security_role_type_id INTEGER NOT NULL,
    UNIQUE (security_user_id, security_role_type_id)
);
