#!/usr/bin/env sh

dropdb --if-exists controlplanedb
createdb controlplanedb

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "controlplanedb" <<-EOSQL
   CREATE USER controlplaneuser WITH PASSWORD 'password';
   ALTER ROLE controlplaneuser SUPERUSER;
EOSQL

dropdb --if-exists warehousedb
createdb warehousedb

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "warehousedb" <<-EOSQL
   CREATE USER warehouseuser WITH PASSWORD 'password';
   ALTER ROLE warehouseuser SUPERUSER;
EOSQL
