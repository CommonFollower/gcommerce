package tech.garymyers.ecommerce.bridge

import io.micronaut.configuration.picocli.PicocliRunner
import picocli.CommandLine.Command
import tech.garymyers.ecommerce.bridge.down.DownCommand
import tech.garymyers.ecommerce.bridge.up.UpCommand

@Command(
   name = "Bridge Command",
   description = ["Bridge for keeping the edge database and cloud database in sync."],
   mixinStandardHelpOptions = true,
   subcommands = [
      DownCommand::class,
      UpCommand::class,
   ]
)
class BridgeCommand : Runnable {
   override fun run() {

   }

   companion object {
      @JvmStatic fun main(args: Array<String>) {
         PicocliRunner.run(BridgeCommand::class.java, *args)
      }
   }
}