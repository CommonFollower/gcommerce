package tech.garymyers.ecommerce.control.plane.api.security

import com.github.javafaker.Faker
import io.micronaut.context.annotation.Requires
import tech.garymyers.ecommerce.control.plane.api.company.Company
import tech.garymyers.ecommerce.control.plane.api.security.role.SecurityRoleTypeEntity
import tech.garymyers.ecommerce.control.plane.api.security.user.SecurityUser
import tech.garymyers.ecommerce.control.plane.api.security.user.infrastructure.SecurityUserRepository

import javax.inject.Singleton
import java.util.stream.IntStream
import java.util.stream.Stream

class SecurityUserTestDataLoader {
   static Stream<SecurityUser> load(int numberIn = 1, Company company, Collection<SecurityRoleTypeEntity> rolesIn) {
      final number = numberIn > 0 ? numberIn : 1
      final faker = new Faker()
      final name = faker.name()
      final lorem = faker.lorem()
      final Collection<SecurityRoleTypeEntity> roles = rolesIn ?: Collections.emptyList()

      return IntStream.range(0, number).mapToObj {
         new SecurityUser(
            null,
            null,
            null,
            name.username(),
            lorem.characters(4, 15, true, true),
            company,
            new LinkedHashSet<SecurityRoleTypeEntity>(roles)
         )
      }
   }
}

@Singleton
@Requires(env = ["test"])
class SecurityUserTestDataLoaderService {
   private final SecurityUserRepository securityUserRepository

   SecurityUserTestDataLoaderService(SecurityUserRepository securityUserRepository) {
      this.securityUserRepository = securityUserRepository
   }

   Stream<SecurityUser> load(int numberIn = 1, Company company, Collection<SecurityRoleTypeEntity> rolesIn) {
      return SecurityUserTestDataLoader.load(numberIn, company, rolesIn)
         .map {
            final password = it.password
            final savedUser = securityUserRepository.save(it)

            return new SecurityUser(
               savedUser.id,
               savedUser.timeCreated,
               savedUser.timeUpdated,
               savedUser.username,
               password,
               company,
               savedUser.roles
            )
         }
   }

   SecurityUser single(Company company, Collection<SecurityRoleTypeEntity> rolesIn = null) {
      return load(1, company, rolesIn).findFirst().orElseThrow { new Exception("Unable to create single Company") }
   }
}
