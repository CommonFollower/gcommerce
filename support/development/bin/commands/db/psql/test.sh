#!/usr/bin/env bash
## description: connect a psql prompt to the Postgres hosted ecomtestdb database

cd ../docker

if [ -z `docker-compose ps -q ecomtestdb` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q ecomtestdb)` ]; then
  echo "ecomtestdb is not running! Not connecting psql."
  exit 1
else
  echo "Connecting interactive psql to ecomtestdb"
  docker-compose build --force-rm --quiet ecombasedb
  docker-compose run --rm ecomtestdbpsql
  exit $?
fi
