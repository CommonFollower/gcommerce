#!/usr/bin/env bash
## description: stops the ecomdb service

cd ../docker

if [ -z `docker-compose ps -q ecomdb` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q ecomdb)` ]; then
  echo "ecomdb is not running"
  exit 1
else
  echo "stopping ecomdb"
  docker-compose stop ecomdb && docker-compose rm -f ecomdb
  exit $?
fi
