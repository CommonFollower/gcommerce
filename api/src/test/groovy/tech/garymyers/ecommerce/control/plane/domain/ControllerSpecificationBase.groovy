package tech.garymyers.ecommerce.control.plane.domain

import io.micronaut.core.type.Argument
import io.micronaut.http.client.BlockingHttpClient
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.security.token.jwt.render.BearerAccessRefreshToken
import tech.garymyers.ecommerce.control.plane.api.company.Company
import tech.garymyers.ecommerce.control.plane.api.company.CompanyTestDataLoaderService
import tech.garymyers.ecommerce.control.plane.api.security.LoginCredentials
import tech.garymyers.ecommerce.control.plane.api.security.SecurityUserTestDataLoaderService
import tech.garymyers.ecommerce.control.plane.api.security.role.Roles
import tech.garymyers.ecommerce.control.plane.api.security.user.SecurityUser

import javax.inject.Inject

import static io.micronaut.http.HttpRequest.*

class ControllerSpecificationBase extends ServiceSpecificationBase {
   @Inject CompanyTestDataLoaderService companyTestDataLoaderService
   @Client("/api") @Inject RxHttpClient httpClient
   @Inject SecurityUserTestDataLoaderService securityUserTestDataLoaderService

   BlockingHttpClient client
   Company company
   SecurityUser adminUser
   String adminToken

   void setup() {
      this.client = httpClient.toBlocking()
      this.company = companyTestDataLoaderService.single()
      this.adminUser = securityUserTestDataLoaderService.single(company, [Roles.ADMIN])
      this.adminToken = loginEmployee(adminUser)
   }

   String loginEmployee(SecurityUser user) {
      return client.exchange(POST("/login", new LoginCredentials(user.username, user.password, user.company.id)), BearerAccessRefreshToken).body().accessToken
   }

   Object get(String path, String accessToken = adminToken) throws HttpClientResponseException {
      return client.exchange(
         GET("/${path}").header("Authorization", "Bearer $accessToken"),
         Argument.of(String),
         Argument.of(String)
      ).bodyAsJson()
   }

   Object post(String path, Object body, String accessToken = adminToken) throws HttpClientResponseException {
      return client.exchange(
         POST("/${path}", body).header("Authorization", "Bearer $accessToken"),
         Argument.of(String),
         Argument.of(String)
      ).bodyAsJson()
   }

   Object put(String path, Object body, String accessToken = adminToken) throws HttpClientResponseException {
      return client.exchange(
         PUT("/${path}", body).header("Authorization", "Bearer $accessToken"),
         Argument.of(String),
         Argument.of(String)
      ).bodyAsJson()
   }

   Object delete(String path, String accessToken = adminToken) throws HttpClientResponseException {
      return client.exchange(
         DELETE("/${path}").header("Authorization", "Bearer $accessToken"),
         Argument.of(String),
         Argument.of(String)
      ).bodyAsJson()
   }
}
