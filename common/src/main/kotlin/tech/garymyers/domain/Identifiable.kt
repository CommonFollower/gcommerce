package tech.garymyers.domain

import java.util.*

interface Identifiable {
   fun myId(): UUID?
}