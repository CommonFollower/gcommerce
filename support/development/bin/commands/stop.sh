#!/usr/bin/env bash
## description: stops all running containers

cd ../docker

docker-compose down
