package tech.garymyers.ecommerce.control.plane.extensions

import io.micronaut.data.repository.CrudRepository
import tech.garymyers.kotlin.extensions.orNull

fun <E, ID> CrudRepository<E, ID>.findByIdOrNull(id: ID): E? =
   this.findById(id!!).orNull()
