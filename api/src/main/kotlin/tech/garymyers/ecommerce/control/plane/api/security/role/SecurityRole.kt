package tech.garymyers.ecommerce.control.plane.api.security.role

import io.micronaut.core.annotation.Introspected
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity

@Introspected
@MappedEntity("security_role_type_domain")
data class SecurityRoleTypeEntity(

   @field:Id
   val id: Int,
   val value: String,
   val description: String,
   val localizationCode: String
)

object Roles {

   @JvmStatic
   val ADMIN = SecurityRoleTypeEntity(1, "SYSTEM_ADMIN", "gCommerce System Administrator", "system.admin")

   @JvmStatic
   val COMPANY_ADMIN = SecurityRoleTypeEntity(2, "COMPANY_ADMIN", "Company Administrator", "company.administrator")

   @JvmStatic
   val WAREHOUSE_MANAGER = SecurityRoleTypeEntity(3, "WAREHOUSE_MANAGER", "Warehouse Manager", "warehouse.manager")

   @JvmStatic
   val WAREHOUSE_PICKER = SecurityRoleTypeEntity(4, "WAREHOUSE_PICKER", "Warehouse Picker", "warehouse.picker")
}
