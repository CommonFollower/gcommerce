#!/usr/bin/env sh

dropdb --if-exists controlplanetestdb
createdb controlplanetestdb

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "controlplanetestdb" <<-EOSQL
   CREATE USER controlplanetestuser WITH PASSWORD 'password';
   ALTER ROLE controlplanetestuser SUPERUSER;
EOSQL

dropdb --if-exists warehousetestdb
createdb warehousetestdb

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "warehousetestdb" <<-EOSQL
   CREATE USER warehousetestuser WITH PASSWORD 'password';
   ALTER ROLE warehousetestuser SUPERUSER;
EOSQL
