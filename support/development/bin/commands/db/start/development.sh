#!/usr/bin/env bash
## description: starts the ecomdevelopdb database and makes it available on localhost:6432 and returns once they are up and running

cd ../docker

daemon="-d"

if [ "$1" == "--nodaemon" ]; then
  daemon=""
fi

if [ -z `docker-compose ps -q ecomdb` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q ecomdb)` ]; then
  docker rm -f ecomdb > /dev/null 2>&1
  docker-compose build --force-rm --quiet ecombasedb
  docker-compose build --force-rm --quiet ecomdb
  docker-compose up $daemon --no-deps ecomdb

  if [ "$daemon" == "-d" ]; then
    docker-compose build --force-rm --quiet ecomdbready && docker-compose run --no-deps --rm ecomdbready
  fi

  exit $?
else
  echo "ecomdb are already running checking if it is accepting connections"
  docker-compose build ecomdbready && docker-compose run --rm ecomdbready
  echo "can be accessed at $(docker-compose port ecomdb 5432)"
  exit 1
fi
