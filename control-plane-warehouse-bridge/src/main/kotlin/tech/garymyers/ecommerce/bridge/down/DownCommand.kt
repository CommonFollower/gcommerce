package tech.garymyers.ecommerce.bridge.down

import picocli.CommandLine.Command

@Command(
   name = "down",
   description = ["Listen for commands from the control plane to replay at the warehouse."],
   mixinStandardHelpOptions = true
)
class DownCommand: Runnable {
   override fun run() {
      TODO("Not yet implemented")
   }
}