#!/usr/bin/env bash
## description: stops the ecomtestdb service

cd ../docker

if [ -z `docker-compose ps -q ecomtestdb` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q ecomtestdb)` ]; then
  echo "ecomtestdb is not running"
  exit 1
else
  echo "stopping ecomtestdb"
  docker-compose stop ecomtestdb && docker-compose rm -f ecomtestdb
  exit $?
fi
