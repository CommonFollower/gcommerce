package tech.garymyers.ecommerce.control.plane.api.security.user.infrastructure

import io.micronaut.data.annotation.Join
import io.micronaut.data.annotation.Query
import io.micronaut.data.annotation.repeatable.JoinSpecifications
import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.repository.PageableRepository
import io.reactivex.Maybe
import org.jdbi.v3.core.Jdbi
import tech.garymyers.ecommerce.control.plane.api.security.user.SecurityUser
import tech.garymyers.kotlin.extensions.getOffsetDateTime
import tech.garymyers.kotlin.extensions.getUuid
import java.util.*
import javax.inject.Inject
import javax.transaction.Transactional

@JdbcRepository
abstract class SecurityUserRepository @Inject constructor(
   private val jdbi: Jdbi,
) : PageableRepository<SecurityUser, UUID> {

   @Transactional
   fun save(user: SecurityUser): SecurityUser {
      return jdbi.withHandle<SecurityUser, Exception> { handle ->
         val savedUser = handle.createQuery(
            """
               INSERT INTO security_user (username, password, company_id)
               VALUES (:username, crypt(:password, gen_salt('bf')), :company_id)
               RETURNING *
            """.trimIndent()
         )
            .bind("username", user.username)
            .bind("password", user.password)
            .bind("company_id", user.company.id)
            .map { rs, _ ->
               SecurityUser(
                  rs.getUuid("id"),
                  rs.getOffsetDateTime("time_created"),
                  rs.getOffsetDateTime("time_updated"),
                  rs.getString("username"),
                  rs.getString("password"),
                  company = user.company,
                  roles = user.roles
               )
            }
            .findOne().orElse(null)

         if (user.roles.isNotEmpty()) {
            val batch = handle.prepareBatch(
               """
                  INSERT INTO security_user_role(security_user_id, security_role_type_id)
                  VALUES(:security_user_id, :security_role_type_id)
               """.trimIndent())
            user.roles.asSequence().forEach { role ->
               batch
                  .bind("security_user_id", savedUser.id)
                  .bind("security_role_type_id", role.id)
                  .add()
            }

            batch.execute()
         }

         savedUser
      }
   }

   @Query(
      """
      SELECT
        su.*,
        c.id                   AS company_id,
        c.time_created         AS company_time_created,
        c.time_updated         AS company_time_updated,
        c.name                 AS company_name,
        srtd.id                AS roles_id,
        srtd.value             AS roles_value,
        srtd.description       AS roles_description,
        srtd.localization_code AS roles_localization_code
      FROM security_user su
           JOIN company c ON su.company_id = c.id
           LEFT OUTER JOIN security_user_role sur ON su.id = sur.security_user_id
           LEFT OUTER JOIN security_role_type_domain srtd ON sur.security_role_type_id = srtd.id
      WHERE su.username = :username
            AND password = crypt(:password, password)
   """
   )
   @JoinSpecifications (
      Join(value = "company"),
      Join(value = "roles")
   )
   abstract fun findByCompanyIdAndUsernameAndPassword(
      companyId: UUID,
      username: String,
      password: String
   ): Maybe<SecurityUser>

   @JoinSpecifications (
      Join(value = "company"),
      Join(value = "roles")
   )
   abstract fun findByIdAndCompanyId(id: UUID, companyId: UUID): SecurityUser?
}