package tech.garymyers.ecommerce.bridge.up

import picocli.CommandLine.Command

@Command(
   name = "up",
   description = ["Listen for changes from the warehouse database, and push them up to the cloud."],
   mixinStandardHelpOptions = true
)
class UpCommand: Runnable {
   override fun run() {
      TODO("Not yet implemented")
   }
}