FROM ecombasedb

COPY ecom-inittestdb.sh /docker-entrypoint-initdb.d/initdb.sh
COPY ecom-inittestdb.sql /docker-entrypoint-initdb.d/initdb.sql
