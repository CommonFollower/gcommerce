package tech.garymyers.kotlin.extensions

import java.util.*

fun <T> Optional<T>.orNull(): T? {
   return if (this.isPresent) {
      this.get()
   } else {
      null
   }
}