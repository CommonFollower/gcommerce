#!/usr/bin/env bash
## description: connect a psql prompt to the Postgres hosted ecomdb database

cd ../docker

if [ -z `docker-compose ps -q ecomdb` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q ecomdb)` ]; then
  echo "ecomdb is not running! Not connecting psql."
  exit 1
else
  echo "Connecting interactive psql to ecomdb"
  docker-compose build --force-rm --quiet ecombasedb
  docker-compose run --rm ecomdbpsql
  exit $?
fi
