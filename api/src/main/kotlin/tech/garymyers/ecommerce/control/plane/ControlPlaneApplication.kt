package tech.garymyers.ecommerce.control.plane

import io.micronaut.runtime.Micronaut

fun main(args: Array<String>) {
   Micronaut.build()
      .args(*args)
      .packages("tech.garymyers.ecommerce.control.plane")
      .start()
}
